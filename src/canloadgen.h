/*
 * canloadgen.h - CAN bus load generator for testing purposes
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2015      Cogent Embedded Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 * Send feedback to <petr.nechaev@cogentembedded.com>
 *
 */
#ifndef CANLOADGEN_H
#define CANLOADGEN_H

#include <signal.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

struct generator_thread;

/**
 * @brief Context of the generator thread.
 * @note No locks or mutexes are used for accessing data because data access is serial by design.
 */
struct generator_thread
{
    /**
     * @brief thread
     */
    pthread_t thread;

    /**
     * @brief timer
     */
    int timer;

    /**
     * @brief Socket for RAW CAN.
     */
    int s;

    /**
     * @brief interval for sending the message
     */
    struct timespec interval;

    /**
     * @brief ifrn_name Interface name, e.g. "en0".
     */
    char ifrn_name[IFNAMSIZ];

    /**
     * @brief frame CAN frame
     */
    struct can_frame frame;

    /**
     * @brief Id of the next frame to be sent.
     */
    uint32_t next_id;

    /**
     * @brief The value to increment Id by.
     */
    uint32_t id_increment;

    /**
     * @brief Counts the number of ENOBUF error.
     */
    uint64_t enobufs_count;

    /**
     * @brief Bus bitrate, needed for calculations.
     */
    int bitrate;

    /**
     * @brief Interval between messages in seconds.
     */
    double message_interval;

    /**
     * @brief Loading of the bus with this message. Can vary from 0.0 to 1.0;
     */
    double pload;

    /**
     * @brief Be verbose.
     */
    bool verbose;

    /**
     * @brief Set to true to stop the timer and the thread.
     */
    volatile bool timer_stopped;

    /**
     * @brief Component Id for logging.
     */
    int component_id;

    /**
     * @brief Initializes data structures of the thread and opens CAN socket.
     */
    void (*open)(struct generator_thread *context);

    /**
     * @brief Sends CAN message.
     */
    void (*send)(struct generator_thread *context);

    /**
     * @brief Closes CAN socket.
     */
    void (*close)(struct generator_thread *context);
};

#endif // CANLOADGEN_H
