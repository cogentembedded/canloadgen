/*
 * canload.h - Main socket operations
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2015      Cogent Embedded Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 * Send feedback to <petr.nechaev@cogentembedded.com>
 *
 */
#ifndef CANLOAD_H
#define CANLOAD_H

#include <stdint.h>
#include <time.h>
#include <stdbool.h>
#include "canloadgen.h"

/**
 * Function in this file try to estimate bitstuffing on CAN bus when calculating available
 * bandwidth so, in reality, the load will be slightly higher or lower than expected.
 *
 * Experiment (tests) should be run to verify resulting CAN bus message composition.
 *
 */

/* Length of CAN message headers */
#define CAN_HEADER_LEN_2_0_A (1 + 11 + 1 + 1 + 1 + 4)
#define CAN_HEADER_LEN_2_0_B (1 + 11 + 1 + 1 + 18 + 1 + 2 + 4)

/* Length of CAN message trailer with inter-frame space */
#define CAN_TRAILER_LEN (15 + 1 + 1 + 1 + 7 + 3)

/**
 * @brief Creates thread context.
 * @param context The context of the thread.
 */
void canload_create(struct generator_thread *context);

#endif // CANLOAD_H
