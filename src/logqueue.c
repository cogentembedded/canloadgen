/*
 * logqueue.c - Queue for non-blocking logging of messages
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2015      Cogent Embedded Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 * Send feedback to <petr.nechaev@cogentembedded.com>
 *
 */
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include "logqueue.h"


struct logQueue
{
    /**
     * @brief POSIX message queue descriptor.
     */
    mqd_t qd;

    /**
     * @brief Log file descriptor.
     */
    FILE *fd;

    /**
     * @brief Set to true to stop the thread.
     */
    volatile bool stopped;

    /**
     * @brief Queue name.
     */
    char name[MAX_STRING_SIZE];

    /**
     * @brief thread.
     */
    pthread_t thread;
};


/**
 * @brief Working queue context.
 */
static struct logQueue *logqueue = NULL;

/**
 * @brief Global variable to avoid memory allocation for logqueue
 */
static struct logQueue def_logqueue;

void* logqueue_thread(void *_)
{
    char s[MAX_STRING_SIZE];
    ssize_t len;

    while (!logqueue->stopped)
    {
        // try to get message from the queue
        len = mq_receive(logqueue->qd, s, MAX_STRING_SIZE, NULL);

        if (len > 0)
        {
            // good
            fputs(s, logqueue->fd);
        }
        else if (!len) {
            fprintf(stderr, "s: EOF.\n", logqueue->name);
            exit(1);
        } else if (errno == EINTR || errno == EAGAIN) {
            usleep(2*1000);
        } else {
            perror(logqueue->name);
            exit(1);
        }
    }

    fclose(logqueue->fd);
    mq_close(logqueue->qd);
}

void logqueue_open(char *filename, char *name)
{
    struct mq_attr attr;

    // assign the queue
    logqueue = &def_logqueue;

    // copy attrs
    strncpy(def_logqueue.name, name, MAX_STRING_SIZE);
    def_logqueue.name[MAX_STRING_SIZE-1] = '\0';

    /* initialize the queue attributes */
    attr.mq_flags = 0;
    attr.mq_maxmsg = 1000; /* the queue is created very large to accommodate spikes */
    attr.mq_msgsize = MAX_STRING_SIZE;
    attr.mq_curmsgs = 0;

    def_logqueue.qd = mq_open(name, O_RDWR  | O_CREAT , 0644, &attr);
    if (def_logqueue.qd == -1)
    {
        perror(name);
        exit(1);
    }

    def_logqueue.fd = fopen(filename, "w");

    // create thread and start it
    pthread_create(&def_logqueue.thread, (pthread_attr_t*)0, logqueue_thread, NULL);

}

void logqueue_close(void)
{
    if (logqueue)
    {
        logqueue->stopped = true;
        logqueue_write("");
        pthread_join(logqueue->thread, NULL);
    }
}

void logqueue_write(char *message)
{
    int ret;
    size_t len = strlen(message);

    if (len > MAX_STRING_SIZE - 1)
        // do not handle complex cases for now
        return;

    if (logqueue)
    {
        // the queue is in operation
        ret = mq_send(logqueue->qd, message, len + 1, 0); /* note: priorities aren't supported */
        if (ret < 0 )
             perror(logqueue->name);
    }
}

