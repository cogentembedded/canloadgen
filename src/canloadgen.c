/*
 * canloadgen.c - CAN bus load generator for testing purposes
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2015      Cogent Embedded Inc.
 * All rights reserved.
 *
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 *
 *
 * Send feedback to <linux-can@vger.kernel.org>
 * Send feedback to <petr.nechaev@cogentembedded.com>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>
#include <ctype.h>
#include <libgen.h>
#include <time.h>
#include <errno.h>
#include <stdbool.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "canloadgen.h"
#include "canload.h"
#include "timings.h"
#include "logqueue.h"

extern int optind, opterr, optopt;

/**
 * @brief Set to 0 to stop the program.
 */
static volatile int running = 1;

#define MAX_THREAD_COUNT 100
#define DEFAULT_BITRATE (500*1000)
#define DEFAULT_COMPONENT_ID 0

void print_usage(char *prg)
{
    fprintf(stderr, "\n%s: generate realistic CAN bus load\n", prg);
    fprintf(stderr, "   version: %s\n", VERSION);
    fprintf(stderr, "\n");
    fprintf(stderr, "Usage: %s [options] <CAN interface>\n", prg);
    fprintf(stderr, "Options: -b <bitrate>   (CAN bus bitrate for timing calculation - default: %d bit/s)\n", DEFAULT_BITRATE);
    fprintf(stderr, "         -o <filename>  (Log CAN messages with timing information into <filename>)\n");
    fprintf(stderr, "         -m <ID:II:LL%> (Configure CAN message with CAN id=<ID> (hex), rated interval = <II> ms, and max bus load of <LL>%.\n");
    fprintf(stderr, "                         Up to %d \"-m\" options may be specified.)\n", MAX_THREAD_COUNT);
    fprintf(stderr, "         -t <n>         (Run for <n> seconds and exit)\n");
    fprintf(stderr, "         -i             (ignore -ENOBUFS return values on write() syscalls)\n");
    fprintf(stderr, "         -c <id>        (Component id for logging - default: %d)\n", DEFAULT_COMPONENT_ID);
    fprintf(stderr, "         -h or -?       (Print this help and exit)\n");
    fprintf(stderr, "         -v             (increment verbosity level for printing a dot \".\" for every CAN frame sent.)\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "%s ensures that unique id are generated in each message.\n", prg);
    fprintf(stderr, "%s uses CLOCK_REALTIME for timestamping.\n", prg);
    fprintf(stderr, "\n");
    fprintf(stderr, "Examples:\n");
    fprintf(stderr, "%s -o /mnt/log/generator.log -b 500000 -m 01:10:10%% -m 02:20:10%% can0 ", prg);
    fprintf(stderr, "(two messages totalling 30%% busload at 500kbps)\n");
    fprintf(stderr, "\n");
}

void sigterm(int signo)
{
    running = 0;
}

bool create_thread_from_opts(struct generator_thread *thread, char *opts)
{
    int id, interval, narg;
    double percent;

    // create thread
    canload_create(thread);

    narg = sscanf(opts, "%x:%u:%lf%%", &id, &interval, &percent);
    if (narg != 3)
        return false;

    // store parameters for future use
    thread->frame.can_id = id;
    thread->pload =  percent / 100.0;
    thread->message_interval = interval/1000.0;

    if (thread->frame.can_id > 0x7FF) {
        printf("The given CAN-ID is greater than 0x7FF. Only CAN 2.0A is supported at the moment.\n");
        return false;
    }

    return true;
}

int main(int argc, char **argv)
{
    unsigned char ignore_enobufs = 0;
    unsigned char verbose = 0;
    int seconds = 0;
    int component_id = DEFAULT_COMPONENT_ID;
    int opt;
    int i;
    int bitrate = DEFAULT_BITRATE;
    char qname[FILENAME_MAX];

    int thread_count = 0;
    struct generator_thread threads[MAX_THREAD_COUNT];

    signal(SIGTERM, sigterm);
    signal(SIGHUP, sigterm);
    signal(SIGINT, sigterm);

    while ((opt = getopt(argc, argv, "ib:c:m:o:t:vh?")) != -1) {
        switch (opt) {

        case 'b':
            bitrate = atoi(optarg);
            break;

        case 'c':
            component_id = atoi(optarg);
            break;

        case 't':
            seconds = atoi(optarg);
            break;

        case 'i':
            ignore_enobufs = 1;
            break;

        case 'o':
            // output file
            snprintf(qname, FILENAME_MAX, "/%s", basename(argv[0]));
            logqueue_open(optarg, qname);
            break;

        case 'm':
            // try to parse new thread options
            thread_count += 1;
            if (thread_count >= MAX_THREAD_COUNT)
            {
                fprintf(stderr, "Sorry, too many messages specified. I support only %d.\n", MAX_THREAD_COUNT);
                exit(1);
            }
            if (!create_thread_from_opts(&threads[thread_count - 1], optarg))
            {
                print_usage(basename(argv[0]));
                return 1;
            }
            break;

        case 'v':
            verbose++;
            break;

        case '?':
        case 'h':
            print_usage(basename(argv[0]));
            return 0;
            break;

        default:
            print_usage(basename(argv[0]));
            return 1;
            break;
        }
    }

    // check that we got interface name and bitrate
    if ( optind == argc || bitrate == 0 || thread_count == 0) {
        print_usage(basename(argv[0]));
        return 1;
    }

    if (strlen(argv[optind]) >= IFNAMSIZ) {
        printf("Name of CAN device '%s' is too long!\n\n", argv[optind]);
        return 1;
    }

    // set common options
    for (i=0; i < thread_count; i++)
    {
        threads[i].bitrate = bitrate;
        threads[i].id_increment = thread_count;
        threads[i].next_id = i;
        threads[i].component_id = component_id;
        threads[i].verbose = verbose > 0;
        strncpy(threads[i].ifrn_name, argv[optind], IFNAMSIZ);
    }

    // start threads
    for (i=0; i < thread_count; i++)
        start_thread(&threads[i]);

    if (seconds)
        // just work for a specific number of seconds if the user told to do so
        sleep(seconds);
    else
        // work forever ;)
        while (running) {
            sleep(1);
        }

    // stop threads
    for (i=0; i < thread_count; i++)
        stop_thread(&threads[i]);

    // close log queue (even if it wasn't open)
    logqueue_close();

    return 0;
}
