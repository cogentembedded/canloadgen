/* 
 * timings.c -timer utilities for can message generator
 *
 * Copyright (c) 2015 Cogent Embedded Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <petr.nechaev@cogentembedded.com>
 */

#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <stdint.h>
#include "timings.h"

static void* timerthread(void *arg)
{
    struct generator_thread * context = arg;
    uint64_t exp;
    ssize_t s;

    // open the context
    context->open(context);

    // create and arm the timer
    context->timer = timerfd_create(CLOCK_MONOTONIC, 0);
    struct itimerspec t;
    t.it_value = context->interval;
    t.it_interval = context->interval;
    timerfd_settime(context->timer, 0, &t, NULL);

    while (!context->timer_stopped) {
        // wait for the timer
        s = read(context->timer, &exp, sizeof(uint64_t));
        if (s != sizeof(uint64_t))
            perror("read");

        if (exp > 1)
            fprintf(stderr, "Missed %llu timer events.\n", exp  - 1);

        // send message through context
        context->send(context);
    }

    context->close(context);

    // destroy the timer
    close(context->timer);

    return NULL;
}

/* Shut down timer & thread */
void stop_thread(struct generator_thread *context)
{
    context->timer_stopped = true;
    pthread_join(context->thread, 0);
}

/* Initialize thread & timer */
void start_thread(struct generator_thread *context)
{
    struct sched_param priority;

    // we're running by default
    context->timer_stopped = false;

    // create thread and start it
    if (pthread_create(&context->thread, (pthread_attr_t*)0, timerthread, (void*)context) < 0)
    {
        perror("pthread_create");
        exit(1);
    }

    // set priority
    priority.__sched_priority = 5;
    if (pthread_setschedparam(context->thread, SCHED_FIFO, &priority) < 0)
    {
        perror("pthread_setschedparam");
        exit(1);
    }
}


