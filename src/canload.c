/*
 * canload.c - Main socket operations
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2015      Cogent Embedded Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 * Send feedback to <petr.nechaev@cogentembedded.com>
 *
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>
#include <ctype.h>
#include <libgen.h>
#include <time.h>
#include <errno.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include "canload.h"
#include "logqueue.h"

/**
 * @brief calc_timer_interval
 * Calculates timer interval for the specific bus load and message length.
 * @param bitrate CAN bus bitrate in bits/s.
 * @param interval Interval between messages in s.
 * @param dlc Data Length Count, the length of CAN data in bytes.
 * @param pload Load percentage from 0.0 to 1.0.
 * @param CAN20B User 29-bit identifier as in CAN 2.0B.
 * @return struct timespec for setting up POSIX timer.
 */
struct timespec calc_timer_interval(int bitrate, double interval, int dlc, bool CAN20B, double pload)
{
    // check arguments

    // calculate message length without stuffing bits
    int framelen = (CAN20B ? CAN_HEADER_LEN_2_0_B : CAN_HEADER_LEN_2_0_A) + dlc * 8 + CAN_TRAILER_LEN;
    // try to add stuffing bits according to http://www.mrtc.mdh.se/publications/0351.pdf
    framelen +=  (dlc / 3 + 5);

    // calculate the required time between messages
    double time = 1 / ( pload * bitrate / framelen );
    // make sure we transmit data every interval
    if (time > interval )
        time = interval;

    fprintf(stderr, "Calculated %lf s interval.\n", time);

    // convert the time to timespec
    struct timespec spec = { .tv_sec = (int)time, .tv_nsec = fmod(time * 1e9 , 1e9) };

    return spec;

}

void canload_open(struct generator_thread *context)
{
    struct sockaddr_can addr;
    struct ifreq ifr;

    if ((context->s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("socket");
        exit(1);
    }

    addr.can_family = AF_CAN;

    strncpy(ifr.ifr_name, context->ifrn_name, IFNAMSIZ);
    if (ioctl(context->s, SIOCGIFINDEX, &ifr) < 0) {
        perror("SIOCGIFINDEX");
        exit(1);
    }
    addr.can_ifindex = ifr.ifr_ifindex;

    /* disable default receive filter on this RAW socket */
    /* This is obsolete as we do not read from the socket at all, but for */
    /* this reason we can remove the receive list in the Kernel to save a */
    /* little (really a very little!) CPU usage.                          */
    setsockopt(context->s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

    int loopback = 0;
    setsockopt(context->s, SOL_CAN_RAW, CAN_RAW_LOOPBACK, &loopback, sizeof(loopback));

    if (bind(context->s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("bind");
        exit(1);
    }

    /* set unused payload data to zero like the CAN driver does it on rx */
    memset(&context->frame.data, 0, CAN_MAX_DLEN);

    // reset performance counts to zero
    context->enobufs_count = 0;

    // calculate the interval for the timer
    context->interval = calc_timer_interval(context->bitrate, context->message_interval, 4, false, context->pload);
}

void canload_send(struct generator_thread *context)
{
    int nbytes;
    char log_msg[128];
    struct timespec abstime;

    // get and increment the id.
    uint32_t id = context->next_id;
    context->next_id += context->id_increment;

    // prepare the frame with our Id field
    context->frame.can_dlc = 4;
    context->frame.data[0] = (id >> 20) & 0xFF;
    context->frame.data[1] = (id >> 12) & 0xFF;
    context->frame.data[2] = (id >> 4) & 0xFF;
    context->frame.data[3] = (id << 4) & 0xFF;

    // send the message and capture the time as fast as possible
    nbytes = write(context->s, &context->frame, CAN_MTU);
    clock_gettime(CLOCK_REALTIME, &abstime);

    // try to handle errors
    if (nbytes < 0)
    {
        if (errno == ENOBUFS)
            context->enobufs_count++;
        else
        {
            // fail on other errors
            perror("write");
            exit(1);
        }
    }
    else if (nbytes < CAN_MTU)
    {
        fprintf(stderr, "write: incomplete CAN frame\n");
        exit(1);
    }

    // success otherwise ;)

    // put the timestamp along with the info message into the log
    snprintf(log_msg,
             sizeof(log_msg),
             "%llu %d %d %d Id %d\n",
             abstime.tv_sec*1000000000ull + abstime.tv_nsec - 1426000000000000000llu,
             context->component_id,
             context->frame.can_id,
             context->frame.can_dlc,
             id);
    logqueue_write(log_msg);
}

void canload_close(struct generator_thread *context)
{
    close(context->s);

    if (context->enobufs_count)
        fprintf(stderr, "\nCounted %llu ENOBUFS return values on write().\n\n", context->enobufs_count);
}


void canload_create(struct generator_thread *context)
{
    // set callback functions
    context->open = &canload_open;
    context->send = &canload_send;
    context->close = &canload_close;
}
